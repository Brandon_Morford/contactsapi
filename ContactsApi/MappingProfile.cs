﻿using System.Collections.Generic;
using AutoMapper;
using Google.Apis.Admin.Directory.directory_v1.Data;
using Newtonsoft.Json.Linq;

namespace ContactsApi
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User, Models.User>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Gender, opt => opt.ResolveUsing(src => GenderMapper(src.Gender)))
                .ForMember(dest => dest.UserAddress, opt => opt.MapFrom(src => src.Addresses))
                .ForMember(dest => dest.UserEmail, opt => opt.MapFrom(src => src.Emails))
                .ForMember(dest => dest.UserExternalId, opt => opt.MapFrom(src => src.ExternalIds))
                .ForMember(dest => dest.UserIm, opt => opt.MapFrom(src => src.Ims))
                .ForMember(dest => dest.UserName, opt => opt.ResolveUsing(src => UserNameMapper(src.Name)))
                .ForMember(dest => dest.UserOrganization, opt => opt.MapFrom(src => src.Organizations))
                .ForMember(dest => dest.UserPhone, opt => opt.MapFrom(src => src.Phones))
                .ForMember(dest => dest.UserRelation, opt => opt.MapFrom(src => src.Relations))
                .ForMember(dest => dest.UserWebsite, opt => opt.MapFrom(src => src.Websites));
            CreateMap<UserName, Models.UserName>()
                .ForMember(dest => dest.UserId, opt => opt.Ignore())
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.User, opt => opt.Ignore());
            CreateMap<UserPhone, Models.UserPhone>()
                .ForMember(dest => dest.UserId, opt => opt.Ignore())
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.User, opt => opt.Ignore()); ;
            CreateMap<UserRelation, Models.UserRelation>()
                .ForMember(dest => dest.UserId, opt => opt.Ignore())
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.User, opt => opt.Ignore()); ;
            CreateMap<UserOrganization, Models.UserOrganization>()
                .ForMember(dest => dest.UserId, opt => opt.Ignore())
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.User, opt => opt.Ignore()); ;
            CreateMap<UserAddress, Models.UserAddress>()
                .ForMember(dest => dest.UserId, opt => opt.Ignore())
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.User, opt => opt.Ignore()); ;
            CreateMap<UserEmail, Models.UserEmail>()
                .ForMember(dest => dest.UserId, opt => opt.Ignore())
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.User, opt => opt.Ignore()); ;
            CreateMap<UserExternalId, Models.UserExternalId>()
                .ForMember(dest => dest.UserId, opt => opt.Ignore())
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.User, opt => opt.Ignore()); ;
            CreateMap<UserIm, Models.UserIm>()
                .ForMember(dest => dest.UserId, opt => opt.Ignore())
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.User, opt => opt.Ignore()); ;
            CreateMap<UserWebsite, Models.UserWebsite>()
                .ForMember(dest => dest.UserId, opt => opt.Ignore())
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.User, opt => opt.Ignore()); ;
            CreateMap<UserGender, Models.Gender>()
                .ForMember(dest => dest.UserId, opt => opt.Ignore())
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.User, opt => opt.Ignore());
        }

        private static List<Models.Gender> GenderMapper(object source)
        {
            if (source == null)
            {
                return new List<Models.Gender>();
            }

            List<Models.Gender> dest = new List<Models.Gender>();
            JToken token = JToken.Parse(source.ToString());
            Models.Gender gender = new Models.Gender()
            {
                Type = token.Value<string>("type"),
                AddressMeAs = token.Value<string>("addressMeAs"),
                CustomGender = token.Value<string>("customGender"),
                Etag = token.Value<string>("eTag")
            };

            dest.Add(gender);
            return dest;
        }

        private static List<Models.UserName> UserNameMapper(UserName source)
        {
            if (source == null)
            {
                return new List<Models.UserName>();
            }

            List<Models.UserName> dest = new List<Models.UserName>();
            Models.UserName name = new Models.UserName()
            {
                Etag = source.ETag,
                FamilyName = source.FamilyName,
                FullName = source.FullName,
                GivenName = source.GivenName,
            };

            dest.Add(name);
            return dest;
        }
    }
}
