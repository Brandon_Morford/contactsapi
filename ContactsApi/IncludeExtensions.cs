﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Reflection;

namespace ContactsApi
{
    public static class IncludeExtensions
    {
        public static IQueryable<T> IncludeAll<T>(this IQueryable<T> queryable) where T : class
        {
            Type type = typeof(T);
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                queryable = queryable.Include(property.Name);
            }
            return queryable;
        }
    }
}
