﻿using System;
using System.Collections.Generic;

namespace ContactsApi.Models
{
    public partial class UserEmail
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Address { get; set; }
        public string CustomType { get; set; }
        public bool? Primary { get; set; }
        public string Type { get; set; }
        public string Etag { get; set; }

        public User User { get; set; }
    }
}
