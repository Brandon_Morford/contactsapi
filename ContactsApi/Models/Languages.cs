﻿using System;
using System.Collections.Generic;

namespace ContactsApi.Models
{
    public partial class Languages
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Language { get; set; }

        public User User { get; set; }
    }
}
