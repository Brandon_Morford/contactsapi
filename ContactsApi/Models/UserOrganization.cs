﻿using System;
using System.Collections.Generic;

namespace ContactsApi.Models
{
    public partial class UserOrganization
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string CostCenter { get; set; }
        public string CustomType { get; set; }
        public string Department { get; set; }
        public string Description { get; set; }
        public string Domain { get; set; }
        public int? FullTimeEquivalent { get; set; }
        public string Location { get; set; }
        public string Name { get; set; }
        public bool? Primary { get; set; }
        public string Symbol { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public string Etag { get; set; }

        public User User { get; set; }
    }
}
