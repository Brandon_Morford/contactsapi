﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ContactsApi.Models
{
    public partial class ContactsContext : DbContext
    {
        public ContactsContext()
        {
        }

        public ContactsContext(DbContextOptions<ContactsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Aliases> Aliases { get; set; }
        public virtual DbSet<CustomSchemas> CustomSchemas { get; set; }
        public virtual DbSet<Gender> Gender { get; set; }
        public virtual DbSet<Keywords> Keywords { get; set; }
        public virtual DbSet<Languages> Languages { get; set; }
        public virtual DbSet<Locations> Locations { get; set; }
        public virtual DbSet<NonEditableAliases> NonEditableAliases { get; set; }
        public virtual DbSet<Notes> Notes { get; set; }
        public virtual DbSet<PosixAccounts> PosixAccounts { get; set; }
        public virtual DbSet<SshPublicKeys> SshPublicKeys { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserAddress> UserAddress { get; set; }
        public virtual DbSet<UserEmail> UserEmail { get; set; }
        public virtual DbSet<UserExternalId> UserExternalId { get; set; }
        public virtual DbSet<UserIm> UserIm { get; set; }
        public virtual DbSet<UserName> UserName { get; set; }
        public virtual DbSet<UserOrganization> UserOrganization { get; set; }
        public virtual DbSet<UserPhone> UserPhone { get; set; }
        public virtual DbSet<UserRelation> UserRelation { get; set; }
        public virtual DbSet<UserWebsite> UserWebsite { get; set; }

        // Unable to generate entity type for table 'dbo.Update'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=E077-N014567L\\SQLEXPRESS;Database=Contacts;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Aliases>(entity =>
            {
                entity.Property(e => e.Alias)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Aliases)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Aliases_User");
            });

            modelBuilder.Entity<CustomSchemas>(entity =>
            {
                entity.Property(e => e.Schema)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.CustomSchemas)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CustomSchemas_User");
            });

            modelBuilder.Entity<Gender>(entity =>
            {
                entity.Property(e => e.AddressMeAs)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CustomGender)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Etag)
                    .HasColumnName("ETag")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Gender)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Gender_User");
            });

            modelBuilder.Entity<Keywords>(entity =>
            {
                entity.Property(e => e.Keyword)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Keywords)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Keywords_User");
            });

            modelBuilder.Entity<Languages>(entity =>
            {
                entity.Property(e => e.Language)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Languages)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Languages_User");
            });

            modelBuilder.Entity<Locations>(entity =>
            {
                entity.Property(e => e.Location)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Locations)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Locations_User");
            });

            modelBuilder.Entity<NonEditableAliases>(entity =>
            {
                entity.Property(e => e.Alias)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.NonEditableAliases)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_NonEditableAliases_User");
            });

            modelBuilder.Entity<Notes>(entity =>
            {
                entity.Property(e => e.Note)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Notes)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Notes_User");
            });

            modelBuilder.Entity<PosixAccounts>(entity =>
            {
                entity.Property(e => e.Account)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.PosixAccounts)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PosixAccounts_User");
            });

            modelBuilder.Entity<SshPublicKeys>(entity =>
            {
                entity.Property(e => e.PublicKey)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.SshPublicKeys)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SshPublicKeys_User");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.CreationTime).HasColumnType("datetime");

                entity.Property(e => e.CreationTimeRaw)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerId)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.DeletionTime).HasColumnType("datetime");

                entity.Property(e => e.DeletionTimeRaw)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Etag)
                    .HasColumnName("ETag")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.HashFunction)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Kind)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.LastLoginTime).HasColumnType("datetime");

                entity.Property(e => e.LastLoginTimeRaw)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.OrgUnitPath)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.PrimaryEmail)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.SuspensionReason)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ThumbnailPhotoEtag)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ThumbnailPhotoUrl)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserAddress>(entity =>
            {
                entity.Property(e => e.Country)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CountryCode)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CustomType)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Etag)
                    .HasColumnName("ETag")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ExtendedAddress)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Formatted)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Locality)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.PoBox)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.PostalCode)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Region)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.StreetAddress)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserAddress)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserAddress_User");
            });

            modelBuilder.Entity<UserEmail>(entity =>
            {
                entity.Property(e => e.Address)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CustomType)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Etag)
                    .HasColumnName("ETag")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserEmail)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserEmail_User");
            });

            modelBuilder.Entity<UserExternalId>(entity =>
            {
                entity.Property(e => e.CustomType)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Etag)
                    .HasColumnName("ETag")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Value)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserExternalId)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserExternalId_User");
            });

            modelBuilder.Entity<UserIm>(entity =>
            {
                entity.Property(e => e.CustomProtocol)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CustomType)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Etag)
                    .HasColumnName("ETag")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Im)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Protocol)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserIm)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserIm_User");
            });

            modelBuilder.Entity<UserName>(entity =>
            {
                entity.Property(e => e.Etag)
                    .HasColumnName("ETag")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.FamilyName)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.FullName)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.GivenName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserName)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserName_User");
            });

            modelBuilder.Entity<UserOrganization>(entity =>
            {
                entity.Property(e => e.CostCenter)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CustomType)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Department)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Domain)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Etag)
                    .HasColumnName("ETag")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Location)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Symbol)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserOrganization)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserOrganization_User");
            });

            modelBuilder.Entity<UserPhone>(entity =>
            {
                entity.Property(e => e.CustomType)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Etag)
                    .HasColumnName("ETag")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Value)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserPhone)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserPhone_User");
            });

            modelBuilder.Entity<UserRelation>(entity =>
            {
                entity.Property(e => e.CustomType)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Etag)
                    .HasColumnName("ETag")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Value)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserRelation)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserRelation_User");
            });

            modelBuilder.Entity<UserWebsite>(entity =>
            {
                entity.Property(e => e.CustomType)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Etag)
                    .HasColumnName("ETag")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Value)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserWebsite)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserWebsite_User");
            });
        }
    }
}
