﻿using System;
using System.Collections.Generic;

namespace ContactsApi.Models
{
    public partial class UserPhone
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string CustomType { get; set; }
        public bool? Primary { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public string Etag { get; set; }

        public User User { get; set; }
    }
}
