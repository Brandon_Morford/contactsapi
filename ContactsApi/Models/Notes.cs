﻿using System;
using System.Collections.Generic;

namespace ContactsApi.Models
{
    public partial class Notes
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Note { get; set; }

        public User User { get; set; }
    }
}
