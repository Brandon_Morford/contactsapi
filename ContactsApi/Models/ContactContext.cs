﻿using System.Collections.Generic;
using Google.Apis.Admin.Directory.directory_v1.Data;
using Microsoft.EntityFrameworkCore;

namespace ContactsApi.Models
{
    public class ContactContext : DbContext
    {
        public ContactContext(DbContextOptions<ContactContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
    }
}
