﻿using System;
using System.Collections.Generic;

namespace ContactsApi.Models
{
    public partial class Gender
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Type { get; set; }
        public string AddressMeAs { get; set; }
        public string CustomGender { get; set; }
        public string Etag { get; set; }

        public User User { get; set; }
    }
}
