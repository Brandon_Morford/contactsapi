﻿using System;
using System.Collections.Generic;

namespace ContactsApi.Models
{
    public partial class CustomSchemas
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Schema { get; set; }

        public User User { get; set; }
    }
}
