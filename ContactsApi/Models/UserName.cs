﻿using System;
using System.Collections.Generic;

namespace ContactsApi.Models
{
    public partial class UserName
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string FamilyName { get; set; }
        public string FullName { get; set; }
        public string GivenName { get; set; }
        public string Etag { get; set; }

        public User User { get; set; }
    }
}
