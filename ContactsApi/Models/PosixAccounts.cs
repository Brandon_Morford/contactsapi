﻿using System;
using System.Collections.Generic;

namespace ContactsApi.Models
{
    public partial class PosixAccounts
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Account { get; set; }

        public User User { get; set; }
    }
}
