﻿using System;
using System.Collections.Generic;

namespace ContactsApi.Models
{
    public partial class User
    {
        public User()
        {
            Aliases = new HashSet<Aliases>();
            CustomSchemas = new HashSet<CustomSchemas>();
            Gender = new HashSet<Gender>();
            Keywords = new HashSet<Keywords>();
            Languages = new HashSet<Languages>();
            Locations = new HashSet<Locations>();
            NonEditableAliases = new HashSet<NonEditableAliases>();
            Notes = new HashSet<Notes>();
            PosixAccounts = new HashSet<PosixAccounts>();
            SshPublicKeys = new HashSet<SshPublicKeys>();
            UserAddress = new HashSet<UserAddress>();
            UserEmail = new HashSet<UserEmail>();
            UserExternalId = new HashSet<UserExternalId>();
            UserIm = new HashSet<UserIm>();
            UserName = new HashSet<UserName>();
            UserOrganization = new HashSet<UserOrganization>();
            UserPhone = new HashSet<UserPhone>();
            UserRelation = new HashSet<UserRelation>();
            UserWebsite = new HashSet<UserWebsite>();
        }

        public int Id { get; set; }
        public string UserId { get; set; }
        public string Kind { get; set; }
        public string LastLoginTimeRaw { get; set; }
        public DateTime? LastLoginTime { get; set; }
        public bool? IsMailboxSetup { get; set; }
        public string OrgUnitPath { get; set; }
        public string Password { get; set; }
        public string PrimaryEmail { get; set; }
        public bool? Suspended { get; set; }
        public string SuspensionReason { get; set; }
        public string ThumbnailPhotoEtag { get; set; }
        public bool? IsEnrolledIn2Sv { get; set; }
        public bool? IsEnforcedIn2Sv { get; set; }
        public bool? IsDelegatedAdmin { get; set; }
        public bool? AgreedToTerms { get; set; }
        public bool? Archived { get; set; }
        public bool? ChangePasswordAtNextLogin { get; set; }
        public string CreationTimeRaw { get; set; }
        public DateTime? CreationTime { get; set; }
        public string CustomerId { get; set; }
        public string DeletionTimeRaw { get; set; }
        public DateTime? DeletionTime { get; set; }
        public string Etag { get; set; }
        public string HashFunction { get; set; }
        public bool? IncludeInGlobalAddressList { get; set; }
        public bool? IpWhitelisted { get; set; }
        public bool? IsAdmin { get; set; }
        public string ThumbnailPhotoUrl { get; set; }

        public ICollection<Aliases> Aliases { get; set; }
        public ICollection<CustomSchemas> CustomSchemas { get; set; }
        public ICollection<Gender> Gender { get; set; }
        public ICollection<Keywords> Keywords { get; set; }
        public ICollection<Languages> Languages { get; set; }
        public ICollection<Locations> Locations { get; set; }
        public ICollection<NonEditableAliases> NonEditableAliases { get; set; }
        public ICollection<Notes> Notes { get; set; }
        public ICollection<PosixAccounts> PosixAccounts { get; set; }
        public ICollection<SshPublicKeys> SshPublicKeys { get; set; }
        public ICollection<UserAddress> UserAddress { get; set; }
        public ICollection<UserEmail> UserEmail { get; set; }
        public ICollection<UserExternalId> UserExternalId { get; set; }
        public ICollection<UserIm> UserIm { get; set; }
        public ICollection<UserName> UserName { get; set; }
        public ICollection<UserOrganization> UserOrganization { get; set; }
        public ICollection<UserPhone> UserPhone { get; set; }
        public ICollection<UserRelation> UserRelation { get; set; }
        public ICollection<UserWebsite> UserWebsite { get; set; }
    }
}
