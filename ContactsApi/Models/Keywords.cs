﻿using System;
using System.Collections.Generic;

namespace ContactsApi.Models
{
    public partial class Keywords
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Keyword { get; set; }

        public User User { get; set; }
    }
}
