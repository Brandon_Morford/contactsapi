﻿using System;
using System.Collections.Generic;

namespace ContactsApi.Models
{
    public partial class SshPublicKeys
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string PublicKey { get; set; }

        public User User { get; set; }
    }
}
