﻿using System;
using System.Collections.Generic;

namespace ContactsApi.Models
{
    public partial class Aliases
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Alias { get; set; }

        public User User { get; set; }
    }
}
