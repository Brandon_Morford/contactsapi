﻿using System;
using System.Collections.Generic;

namespace ContactsApi.Models
{
    public partial class UserAddress
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string CustomType { get; set; }
        public string ExtendedAddress { get; set; }
        public string Formatted { get; set; }
        public string Locality { get; set; }
        public string PoBox { get; set; }
        public string PostalCode { get; set; }
        public bool? Primary { get; set; }
        public string Region { get; set; }
        public bool? SourceIsStructured { get; set; }
        public string StreetAddress { get; set; }
        public string Type { get; set; }
        public string Etag { get; set; }

        public User User { get; set; }
    }
}
