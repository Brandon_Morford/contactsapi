﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Google.Apis.Admin.Directory.directory_v1;
using Google.Apis.Admin.Directory.directory_v1.Data;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Util.Store;

namespace ContactsApi
{
    public static class ContactService
    {
        static readonly string[] Scopes = { DirectoryService.Scope.AdminDirectoryUserReadonly, DirectoryService.Scope.AdminDirectoryUser};
        static readonly string ApplicationName = "Contacts API";
        static readonly string Domain = "wyo.gov";

        static ContactService()
        {
            //GoogleCredential credential;

            //using (FileStream stream = new FileStream("ContactsApi.json", FileMode.Open, FileAccess.Read))
            //{
            //    credential = GoogleCredential.FromStream(stream)
            //        .CreateScoped(Scopes)
            //        .CreateWithUser("contactsapiservice@contactsapi-1538676548381.iam.gserviceaccount.com");
            //}

            //// Create Directory API service.
            //Service = new DirectoryService(new BaseClientService.Initializer()
            //{
            //    HttpClientInitializer = credential,
            //    ApplicationName = ApplicationName,
            //    ApiKey = "41898dc60cef95a20e78e94de8c16ac82489fc25",
            //});

            UserCredential credential;

            using (FileStream stream = new FileStream("credentials.json", FileMode.Open, FileAccess.Read))
            {
                string credPath = "token.json";
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
            }

            // Create Directory API service.
            Service = new DirectoryService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName
            });
        }

        public static List<User> GetAll()
        {
            List<User> result = new List<User>();
            UsersResource.ListRequest request = Service.Users.List();
            request.ViewType = UsersResource.ListRequest.ViewTypeEnum.DomainPublic;
            request.Domain = Domain;

            do
            {
                Users response = request.Execute();
                result.AddRange(response.UsersValue);
                request.PageToken = response.NextPageToken;
            }
            while (!string.IsNullOrEmpty(request.PageToken));
            

            return result;
        }

        /// <summary>
        /// Create a watch request for the User resource.
        /// </summary>
        /// <param name="id">A UUID to identify the channel.</param>
        /// <param name="address">The callback URL for the response.</param>
        /// <param name="event">The event type to create a watch for.</param>
        public static void Watch(string id, string address, UsersResource.WatchRequest.EventEnum @event)
        {
            Channel channel = new Channel
            {
                Address = address,
                Id = id
            };
            UsersResource.WatchRequest request = Service.Users.Watch(channel);
            request.Projection = UsersResource.WatchRequest.ProjectionEnum.Full;
            request.ViewType = UsersResource.WatchRequest.ViewTypeEnum.DomainPublic;
            request.Domain = Domain;
            request.Event = @event;

            Channel response = request.Execute();
        }

        /// <summary>
        /// Create a watch request using default parameters for local debugging.
        /// </summary>
        public static void Watch()
        {
            const string callbackAddress = "https://localhost:44325/Watch";
            string uuid = Guid.NewGuid().ToString();

            Watch(uuid, callbackAddress, UsersResource.WatchRequest.EventEnum.Add);

            uuid = Guid.NewGuid().ToString();
            Watch(uuid, callbackAddress, UsersResource.WatchRequest.EventEnum.Update);
        }

        private static DirectoryService Service { get; set; }
    }
}
