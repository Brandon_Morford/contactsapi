﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Reflection;
using AutoMapper;
using ContactsApi.Models;
using Microsoft.AspNetCore.Mvc;
using User = Google.Apis.Admin.Directory.directory_v1.Data.User;

namespace ContactsApi.Controllers
{
    [Route("api/Contacts")]
    [ApiController]
    public class ContactsController : ControllerBase
    {
        private readonly ContactsContext _context;
        private readonly IMapper _mapper;

        public ContactsController(ContactsContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;


            // TODO: Update this to use a watch.
            if (_context.User.Count() < 1)
            {
                List<User> userList = ContactService.GetAll();
                List<Models.User> dbMappedUsers = _mapper.Map<List<Models.User>>(userList);
                _context.User.AddRange(dbMappedUsers);

                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    foreach (DbEntityValidationResult error in ex.EntityValidationErrors)
                    {
                        Console.WriteLine("====================");
                        Console.WriteLine("Entity {0} in state {1} has validation errors:",
                            error.Entry.Entity.GetType().Name, error.Entry.State);
                        foreach (DbValidationError ve in error.ValidationErrors)
                        {
                            Console.WriteLine("\tProperty: {0}, Error: {1}",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                        Console.WriteLine();
                    }
                    throw;
                }
            }
        }

        [HttpGet]
        public ActionResult<List<Models.User>> GetAll()
        {
            return _context.User.IncludeAll().ToList();
        }

        [HttpGet("Id/{id}")]
        public ActionResult<Models.User> GetById(string id)
        {
            Models.User item = _context.User
                .FirstOrDefault(i => i.UserId == id);

            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        [HttpGet("Email/{email}")]
        public ActionResult<List<Models.User>> GetByEmail(string email)
        {
            List<Models.User> item = _context.User
                .Where(i => i.UserEmail
                .Any(e => e.Address.Contains(email)))
                .ToList();

            if (item.Count() == 0)
            {
                return NotFound();
            }
            return item;
        }

        [HttpGet("Search/{query}")]
        public ActionResult<List<Models.User>> GetBySearch(string query)
        {
            Type type = _context.User.GetType().GetGenericArguments()[0];
            PropertyInfo[] properties = type.GetProperties();
            List<Models.User> item = _context.User
                .AsEnumerable()
                .Where(i => properties
                .Any(p =>
                {
                    object value = p.GetValue(i);
                    return value != null && value.ToString().Contains(query);
                })).ToList();

            if (item.Count() == 0)
            {
                return NotFound();
            }
            return item;
        }
    }
}
